import hero from './hero.jpg';
import barChart from './bar-chart.png';
import dietitian from './dietitian.png';
import promotions from './promotions.png';

export const images = {
  hero,
  barChart,
  dietitian,
  promotions
}
