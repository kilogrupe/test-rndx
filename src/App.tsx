import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {RouteKeys} from "./constants/routeKeys";
import {Home} from './routes/Home';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={RouteKeys.home} component={Home}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
