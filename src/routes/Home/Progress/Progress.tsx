import React from 'react';
import style from './index.module.scss';

interface Props {
  step:number
}
export const Progress = (props:Props) => {
  const {step} = props;
  return (
    <div className={style.progress}>
      <div className={style.container}>
        <div className={`${style.dot} ${style.active}`}>
          <span className={`${style.text} ${style.active}`}>ACCOUNT CREATED</span>
        </div>
        <div className={`${style.line} ${step===2 && style.active}`}/>
        <div className={`${style.dot} ${step===2 && style.active}`}>
          <span className={`${style.text} ${step===2 && style.active}`}>SPECIAL OFFER</span>
        </div>
        <div className={style.line}/>
        <div className={style.dot}>
          <span className={style.text}>ORDER RECEIPT</span>
        </div>
      </div>
    </div>
  );
};
