import React from 'react';
import style from './index.module.scss';
import {images} from "../../../assets/images/images";
import {List} from "../../../components/List";

export const Promotion = () => {
  const content = [
    'A 28-day fasting plan made by experts',
    'Healthy eating tips and strategies',
    'Your personal progress tracking sheet',
    'Daily emails that will make your fasting challenge easy',
  ]
  return (
    <div className={style.promotions}>
      <div className={style.container}>
        <h1 className={style.heading}><strong>ONE-TIME OPPORTUNITY</strong> to Get 28-Day <br/>Fitter Fasting Plan Made By Experts</h1>
        <img src={images.promotions} className={style.image} alt="cards"/>
        <List list={content} heading='Enroll now and get:'/>
      </div>
    </div>
  );
};
