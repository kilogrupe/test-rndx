import React from 'react';
import style from './index.module.scss';

interface Props {
  className?: string;
  list: string[];
  heading: string;
}

export const List = (props: Props) => {
  const {className, list, heading} = props;
  const itemsList =list.map((text,i)=>{
      return (
        <li key={i} className={style.item}>{text}</li>
      )
  })
  return (
    <div className={`${style.container} ${className}`}>
      <h2 className={style.heading}>
        {heading}
      </h2>
      <ul className={style.list}>
        {itemsList}
      </ul>
    </div>
  );
};
