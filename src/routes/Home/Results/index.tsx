import React from 'react';
import style from './index.module.scss';

interface Props {
  value:number
}
export const Results = (props:Props) => {
  const {value} = props;
  return (
    <div className={style.results}>
      <div className={style.value}>{value}%</div>
      <div className={style.desc}>
        <span>better weight</span>
        <span>loss results</span>
      </div>
    </div>
  );
};
