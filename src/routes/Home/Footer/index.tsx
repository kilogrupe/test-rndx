import React from 'react';
import style from './index.module.scss';
import {List} from "../../../components/List";

export const Footer = () => {
  const content = [
    'You receive an email with all the necessary info to start Fitter Fasting Challenge',
    'You will be enrolled in the email sequence which will guide you with tips & tricks throughout the challenge',
  ]
  return (
    <div className={style.footer}>
      <List list={content} heading='What happens after I order?'/>
    </div>
  );
};
