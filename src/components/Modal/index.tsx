import React, {useEffect} from 'react';
import style from './index.module.scss';

interface Props {
  isVisible: boolean;
  toggleModal: (value:boolean) => void
}

export const Modal = (props: Props) => {
  const {isVisible, toggleModal} = props;
  useEffect(()=>{
    document.addEventListener('keydown', (e)=>{
      if(e.keyCode===27) toggleModal(false)
    })
  })
  if (!isVisible) return null
  return (
    <>
      <div className={style.modal}>
        <h2>Modal</h2>
      </div>
      <div className={style.backdrop} onClick={()=>toggleModal(false)}/>
    </>

  );
};
