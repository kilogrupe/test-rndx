import React from 'react';
import style from './index.module.scss';
import {images} from "../../../assets/images/images";

export const Description = () => {
  return (
    <div className={style.description}>
      <div className={style.container}>
        <div className={style.row}>
          <img src={images.barChart} className={style.icon} alt="bars"/>
          <p className={style.text}>
            If you’re looking to multiply your results, this <strong>
            28-day intermittent fasting challenge along with physical activity will double your fat-burning and autophagy processes as well as anti-aging effects.
          </strong>
          </p>
        </div>
        <div className={style.table}>
          <p className={style.text}>
            Our 28-day fasting challenge is necessary for anyone trying to lose weight and get fit.
          </p>
          <p className={style.important}>
            During this challenge, you will also notice improved energy levels, decreased anxiety, and overall improved mood.
          </p>
        </div>
      </div>
    </div>
  );
};
