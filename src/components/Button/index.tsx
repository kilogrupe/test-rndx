import React from 'react';
import style from './index.module.scss';

interface Props {
  text: string;
  onClick: () => void;
  className?: string;
}

export const Button = (props: Props) => {
  const {text, onClick, className} = props;
  return (
    <div onClick={onClick} className={`${style.button} ${className}`}>
      {text}
    </div>
  );
};
