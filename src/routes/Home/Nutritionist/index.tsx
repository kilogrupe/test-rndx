import React from 'react';
import style from './index.module.scss';
import {images} from "../../../assets/images/images";

export const Nutritionist = () => {
  return (
    <div className={style.dietitian}>
      <div className={style.container}>
        <img className={style.image} src={images.dietitian} alt="dietitian"/>
        <h2 className={style.heading}>
          Fitter Fasting challenge with professional nutritionist Christine Ellis
        </h2>
        <p className={style.desc}>
          To make it effortless for you, our professional nutritionist Christine Ellis will guide you through each
          challenge day.
        </p>
      </div>
    </div>
  );
};
