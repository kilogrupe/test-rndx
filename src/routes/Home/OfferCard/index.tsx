import React from 'react';
import style from './index.module.scss';
import {Button} from "../../../components/Button";

interface Props {
  price: number;
  save: number;
  normalPrice: number;
  nextPrice: number;
  showModal: (value:boolean)=>void
}

export const OfferCard = (props: Props) => {
  const {price, save, nextPrice, normalPrice,showModal} = props;
  return (
    <div className={style.offer_card}>
      <div className={style.container}>
        <h1 className={style.heading}>More than <strong>10.000+</strong> took this offer</h1>
        <div className={style.card}>
          <div className={style.save}>
            Save -{save}%
          </div>
          <h3 className={style.price}>{price}</h3>
          <p className={style.normal_price}>Normal retail price ${normalPrice}</p>
          <div className={style.one_time_payment}>ONE-TIME PAYMENT</div>
          <h3 className={style.one_time_opportunity}>ONE-TIME OPPORTUNITY</h3>
          <p className={style.availability}>This offer won’t be available again</p>
        </div>
        <Button onClick={()=>showModal(true)} text={`Buy now only ${nextPrice}`} className={style.button}/>
        <p className={style.fees}>
          This is not a subscription. There are no hidden fees. No contracts.
        </p>
      </div>
    </div>
  );
};
