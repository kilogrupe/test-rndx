import React from 'react';
import style from './index.module.scss';
import {images} from "../../../assets/images/images";

export const Hero = () => {
  return (
    <div className={style.hero}>
      <div className={style.container}>
        <p className={style.before}>Wait!</p>
        <h1 className={style.heading}>
          Last but not least, double your weight loss with
          <br/>
          <strong>PERSONALIZED 28-day intermittent fasting challenge.</strong>
        </h1>
        <div className={style.content}>
          <img className={style.image} src={images.hero} alt="hero"/>
          <p className={style.text}>
            <strong>Fact:</strong> Physical activity combined with intermittent fasting diet is <strong>the most effective strategy for sustainable weight loss.</strong> It promotes rapid calorie and fat burning processes.
          </p>
        </div>
      </div>
    </div>
  );
};
