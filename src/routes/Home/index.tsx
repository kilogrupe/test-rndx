import React, {useState} from 'react';
import style from './index.module.scss';
import {TimedOffer} from "./TimedOffer";
import {Progress} from "./Progress/Progress";
import {Hero} from "./Hero";
import {Results} from "./Results";
import {Description} from "./Description";
import {Nutritionist} from "./Nutritionist";
import {Benefits} from "./Benefits";
import {Promotion} from "./Promotion";
import {OfferCard} from "./OfferCard";
import {Footer} from "./Footer";
import {Modal} from "../../components/Modal";

export const Home = () => {
  const [isVisible, toggleModal] = useState(false)
  return (
    <div className={style.home}>
      <Modal isVisible={isVisible} toggleModal={toggleModal}/>
      <TimedOffer toggleModal={toggleModal}/>
      <Progress step={2}/>
      <Hero/>
      <Results value={92}/>
      <Description/>
      <Nutritionist/>
      <Benefits/>
      <Promotion/>
      <OfferCard showModal={toggleModal} nextPrice={9.99} normalPrice={79.99} price={19.99} save={75}/>
      <Footer/>
    </div>
  );
};
