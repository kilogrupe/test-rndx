import React, {useEffect, useState} from 'react';
import style from './index.module.scss';
import {Button} from "../../../components/Button";

interface Props {
  toggleModal: (value: boolean) => void;
}

export const TimedOffer = (props: Props) => {
  const [minutes, setMinutes] = useState(60)
  const [seconds, setSeconds] = useState(60);

  const {toggleModal} = props;
  useEffect(() => {
    let sec = seconds;
    setInterval(() => {
      sec-=1
      setSeconds(sec)
    }, 1000)
  }, [])
  return (
    <div className={style.timed_offer}>
      <div className={style.container}>
        <div className={style.date}>
          <p className={style.valid}>This offer is valid for:</p>
          <div className={style.hours}>
            <strong>00</strong>
            <span>hours</span>
          </div>
          <div className={style.minutes}>
            <strong>14</strong>
            <span>minutes</span>
          </div>
          <div className={style.seconds}>
            <strong>{seconds}</strong>
            <span>seconds</span>
          </div>
        </div>
        <Button className={style.button} onClick={() => toggleModal(true)} text='BUY NOW'/>
      </div>
    </div>
  );
};
