import React from 'react';
import style from './index.module.scss';
import {List} from "../../../components/List";

export const Benefits = () => {
  const content = [
    'Weight loss',
    'It can reduce insulin resistance, lowering blood sugar by 3-6% and insulin levels by 20-32%',
    'Reduces risk of inflammation',
    'Reduces “bad” LDL cholesterol, blood triglycerides, inflammatory markers and blood sugar',
    'Increases the brain hormone BDNF and may aid the growth of new nerve cells',
  ]
  return (
    <div className={style.benefits}>
      <div className={style.container}>
        <List list={content} heading='Intermittent fasting benefits'/>
      </div>
    </div>
  );
};
